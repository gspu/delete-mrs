import axios from 'axios';
import { config } from './config';
import { GitLabResponseType } from './gitlab-response';

const deleteMergeRequests = async (): Promise<void> => {
  const res = (
    await axios.get(`https://gitlab.com/api/v4/projects/${config.projectID}/merge_requests`, {
      headers: { 'PRIVATE-TOKEN': config.token },
    })
  ).data as GitLabResponseType[];
  const references = res.map(e => e.reference.split('!')[1]);
  if (res.length > 0) {
    await Promise.all(
      references.map(async e =>
        axios.delete(`https://gitlab.com/api/v4/projects/${config.projectID}/merge_requests/${e}`, {
          headers: { 'PRIVATE-TOKEN': config.token },
        }),
      ),
    );
    await deleteMergeRequests();
  }
  return;
};

if (require.main) {
  deleteMergeRequests().catch((e: Error) => {
    console.log(`${e.message}`);
  });
}
